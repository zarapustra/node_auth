// load the things we need
var bcrypt = require('bcrypt-nodejs');
var Sequelize = require('sequelize');
var sequelize = require('../config/db');
var tokenizer = require('../lib/tokenizer');

var User = sequelize.define('user', {
    username: Sequelize.STRING,
    encrypted_password: Sequelize.STRING,
    authentication_token: Sequelize.STRING,
    //created_at: Sequelize.TIME,
    //updated_at: Sequelize.TIME,
    auth: Sequelize.JSONB
}, {
    timestamps: false,
    instanceMethods: {
        validPassword: function (password) {
            return bcrypt.compareSync(password, this.encrypted_password);
        },
        encToken: function () {
          console.log(this.authentication_token.length)
          return tokenizer.encode(this.authentication_token)
        }
    },
    classMethods: {
        encryptPassword: function(password) {
            return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
        },
        generatePassword: function() {
            const params = require('../config/app'),
                  length = params.passLength,
                  chars = "abcdefghijklmnopqrstuvwxyz1234567890";

            var pass = "";
            for (var x = 0; x < length; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            return pass;
        },
        authorizedByEncToken: function(encToken) {
            var token = tokenizer.decode(encToken);
            var user = User.findByToken(token);
            if (user) {
                return true;
            } else {
                return false;
            }
        },
        findByToken: function(token) {
            User.findOne({
                where: {
                    "authentication_token": {
                        $eq: token
                    }
                }
            }).then(
                user => {
                    return user;
                }
            );
        }
    }
});
module.exports = User;
