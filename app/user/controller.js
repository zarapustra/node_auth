var User = require('./model');
var sequelize = require('../config/db');
var tokenizer = require('../lib/tokenizer');
const notifier = require('../lib/mail/signup');

exports.create = function(attrs) {
    var password = User.generatePassword();
    var userParams = {
        username: attrs.email,
        encrypted_password: User.encryptPassword(password),
        authentication_token: tokenizer.generateToken(),
    }
    if (attrs.provider && attrs.uid) {
        userParams.auth[attrs.provider] = {
            uid: attrs.uid
        }
    }
    return User.create(userParams).then(
        user => {
            notifier(user.dataValues.username, password); // отправляем пароль
            return user;
        }, err => {
            return err;
        }
    );
}

exports.update = function(attrs) {
        console.log('uid: ' + attrs.uid);
        var json = {
            authentication_token: tokenizer.generateToken(),
            auth: {}
        }
        json.auth[attrs.provider] = { uid: attrs.uid }
        return attrs.user.update(json);
}
