const AWS = require('aws-sdk');
const awsParams =
  {
    accessKeyId: process.env.MAIN_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.MAIN_AWS_SECRET_ACCESS_KEY,
    region: 'eu-central-1'
  }
AWS.config.update(awsParams);

var sns = new AWS.SNS();

module.exports = sns;
