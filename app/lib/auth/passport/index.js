
// load up the user model
var User       = require('../../../user/model');
var sequelize  = require('../../../config/db');

module.exports = function(passport) {

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id).then(
          user => {
            done(null, user);
          }, err => {
            done(err, null);
          }
        );
    });

    var LocalStrategy    = require('passport-local').Strategy;
    // web
    require('./signin')(passport, LocalStrategy);
    require('./signup')(passport, LocalStrategy);

    // load the auth variables
    var configAuth = require('../../../config/auth'); // use this one for testing

    require('./fb')(passport, configAuth);
    require('./google')(passport, configAuth);
    //require('./web/vk')(passport, configAuth);
    //require('./web/twi')(passport, configAuth);
};
