// =========================================================================
// LOCAL SIGNUP ============================================================
// =========================================================================
const UserController = require('../../../user/controller');
module.exports = function(passport, LocalStrategy) {
    passport.use('local-signup', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
        },
        function(req, email, password, done) {
            if (email)
                email = email.toLowerCase(); // Use lower-case e-mails to avoid case-sensitive e-mail matching
            // asynchronous
            process.nextTick(function() {
                if (!req.user) {
                    User.findOne({
                        where: {
                            "email": {
                                $eq: email
                            }
                        }
                    }).then(
                        user => {
                            if (user) {
                                return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
                            } else {
                                // create the user
                                return UserController.create({
                                    email: email
                                }).then(
                                    user => {
                                        return done(null, user);
                                    },
                                    err => {
                                        return done(err);
                                    }
                                );
                            }
                        },
                        error => {
                            return done(error);
                        });
                } else if (!req.user.email) {
                    // ...presumably they're trying to connect a local account
                    // BUT let's check if the email used to connect a local account is being used by another user
                    User.findOne({
                        where: {
                            "email": {
                                $eq: email
                            }
                        }
                    }).then(
                        user => {
                            if (user) {
                                return done(null, false, req.flash('loginMessage', 'That email is already taken.'));
                                // Using 'loginMessage instead of signupMessage because it's used by /connect/local'
                            } else {
                                sequelize.sync().then(function() {
                                        return User.create({
                                            info: {
                                                local: {
                                                    email: email,
                                                    password: User.encryptedPassword(password)
                                                }
                                            }
                                        }).then(
                                            user => {
                                                console.log(user);
                                                return done(null, user);
                                            },
                                            err => {
                                                return done(err);
                                            }
                                        );
                                    },
                                    error => {
                                        return done(error);
                                    });
                            }
                        },
                        err => {
                            return done(err);
                        }
                    );
                } else {
                    // user is logged in and already has a local account. Ignore signup. (You should log out before trying to create a new account, user!)
                    return done(null, req.user);
                }
            });
        }));
}
