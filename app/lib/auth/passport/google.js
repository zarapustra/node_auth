var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
module.exports = function(passport, configAuth) {

    // =========================================================================
    // GOOGLE ==================================================================
    // =========================================================================
    passport.use('google', new GoogleStrategy({

            clientID: configAuth.googleAuth.clientID,
            clientSecret: configAuth.googleAuth.clientSecret,
            callbackURL: configAuth.googleAuth.callbackURL,
            passReqToCallback: true // allows us to pass in the req from our route (lets us check if a user is logged in or not)

        },
        function(req, token, refreshToken, profile, done) {

            // asynchronous
            process.nextTick(function() {

                // check if the user is already logged in
                if (!req.user) {

                    User.findOne({
                        'google.id': profile.id
                    }, function(err, user) {
                        if (err)
                            return done(err);

                        if (user) {

                            // if there is a user id already but no token (user was linked at one point and then removed)
                            if (!user.info.google.token) {
                                user.info.google.token = token;
                                user.info.google.name = profile.displayName;
                                user.info.google.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                                user.save(function(err) {
                                    if (err)
                                        return done(err);

                                    return done(null, user);
                                });
                            }

                            return done(null, user);
                        } else {
                            var newUser = new User();

                            newUser.info.google.id = profile.id;
                            newUser.info.google.token = token;
                            newUser.info.google.name = profile.displayName;
                            newUser.info.google.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                            newUser.save(function(err) {
                                if (err)
                                    return done(err);

                                return done(null, newUser);
                            });
                        }
                    });

                } else {
                    // user already exists and is logged in, we have to link accounts
                    var user = req.user; // pull the user out of the session

                    user.info.google.id = profile.id;
                    user.info.google.token = token;
                    user.info.google.name = profile.displayName;
                    user.info.google.email = (profile.emails[0].value || '').toLowerCase(); // pull the first email

                    user.save(function(err) {
                        if (err)
                            return done(err);

                        return done(null, user);
                    });

                }

            });

        }));
};
