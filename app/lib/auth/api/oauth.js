const User = require('../../../user/model');
const graphPromise = require('../../graph/fabric');
const tokenizer = require('../../tokenizer');
const UserController = require('../../../user/controller');

module.exports = function(provider, access_token) {
    // ищем юзера в базе по email из графа
    return graphPromise(provider, access_token).then(
        graph => {
            if (!graph) {
                return {
                    status: 401
                }
            }
            return User.findOne({
                where: {
                    "username": {
                        $eq: graph.email
                    }
                }
            }).then(
                user => {
                    if (user) {
                        //console.log(user.auth[provider])
                        if (user.auth && user.auth[provider].uid === graph.uid) {
                            return {
                                status: 302,
                                token: user.encToken()
                            };
                        } else {
                            return UserController.update({
                                user: user,
                                provider: provider,
                                uid: graph.uid
                            }).then(res => {
                                return {
                                    status: 302,
                                    token: user.encToken()
                                }
                            });
                        }
                    } else {
                        // создаем пользователя c uid, token
                        return UserController.create({
                                email: graph.email,
                                provider: provider,
                                uid: graph.uid
                            })
                            .then(
                                user => {
                                    return {
                                        status: 201,
                                        token: user.encToken()
                                    };
                                },
                                err => {
                                    return err;
                                }
                            );
                    }
                }, err => {
                    return err;
                });
        })
};

// если юзер найден - отдаем код 406
// если юзер не найден
// генерим пароль
// криптуем пароль
// генерим токен
// создаем нового юзера с мылом, с криптованным паролем, токеном
// отправляем мыло с паролем
// генерим охуевший токен
