// =========================================================================
// API SIGNIN ==============================================================
// =========================================================================
const UserController = require('../../../user/controller');
const User = require('../../../user/model');
module.exports = function(email, password) {
    return User.findOne({
        where: {
            "username": {
                $eq: email
            }
        }
    }).then(
        user => {
            if (!user)
                return {
                    status: 404
                }
            if (user.validPassword(password))
                return {
                    status: 201,
                    token: user.encToken()
                }
            else
                return {
                    status: 401
                }
        });
}
