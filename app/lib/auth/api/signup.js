// =========================================================================
// API SIGNUP ==============================================================
// =========================================================================
const UserController = require('../../../user/controller');
const User = require('../../../user/model');
module.exports = function(email) {
    return User.findOne({
        where: {
            "username": {
                $eq: email
            }
        }
    }).then(
        user => {
            if (user) {
                return {
                    status: 406
                }
            } else {
                return UserController.create({
                    email: email
                }).then(
                    user => {
                        return {
                            status: 201,
                            token: user.encToken()
                        }
                    }
                );
            }
        });
}
