module.exports = function(provider, token) {
    const fb = require('./fb');
    const google = require('./google');
    const vk = require('./vk');

    switch (provider) {
        case 'facebook':
            {
                return fb(token).then(
                    graph => {
                        return {
                            uid: graph.uid,
                            fullName: [graph.first_name, graph.last_name].join(' '),
                            email: graph.email
                        };
                    });
            }
            break;

        case 'google':
            {
                return google(token).then(
                    graph => {
                        if (graph) {
                            return {
                                uid: graph.sub,
                                fullName: [graph.given_name, graph.family_name].join(' '),
                                email: graph.email
                            };
                        }
                    });
            }
            break;

        case 'vk':
            {
                return vk(token).then(
                    (graph) => {
                        return {
                            uid: graph.uid,
                            fullName: [graph.first_name, graph.last_name].join(' '),
                            email: graph.email
                        };
                    });
            }
            break;

        default:
            return {}
    }
}
