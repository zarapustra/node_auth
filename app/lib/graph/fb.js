module.exports = function(token) {
  var rp = require('request-promise');
  var options = {
      uri: 'https://graph.facebook.com/me',
      qs: {
          access_token: token
      },
      headers: {
          'User-Agent': 'Request-Promise'
      },
      json: true
  };

  return rp(options)
      .then(function (graph) {
          return graph;
      })
      .catch(function (err) {
          console.log('FB API call failed: ' + err);
      });
}
