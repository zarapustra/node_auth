module.exports = function(token) {
  var rp = require('request-promise');
  var options = {
      uri: 'https://api.vk.com/method/users.get',
      qs: {
          access_token: token
      },
      headers: {
          'User-Agent': 'Request-Promise'
      },
      json: true
  };

  return rp(options)
      .then(function (graph) {
          return graph.response[0];
      })
      .catch(function (err) {
          console.log('VK API call failed: ' + err);
      });
}
