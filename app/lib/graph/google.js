module.exports = function(token) {
  var rp = require('request-promise');
  var options = {
      uri: 'https://www.googleapis.com/oauth2/v3/userinfo',
      qs: {
          alt: 'json',
          access_token: token
      },
      headers: {
          'User-Agent': 'Request-Promise'
      },
      json: true
  };

  return rp(options)
      .then(function (graph) {
          return graph;
      })
      .catch(function (err) {
          console.log('Google API call failed: ' + err);
      });
}
