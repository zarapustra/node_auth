module.exports = function(os, push_token, msg, origin) {
  const sns = require('../../config/sns');

  function platform_for(os) {
    switch (os)
    {
       case 'ios': {
         return process.env.IOS_PROD_PLATFORM_APP_ARN
       }
       break;

       case 'android': {
         console.log(process.env.ANDROID_PLATFORM_APP_ARN)
         return process.env.ANDROID_PLATFORM_APP_ARN
       }
       break;

       default: return {}
    }
  }

      params = {
          PlatformApplicationArn: platform_for(os),
          Token: push_token
        }

      sns.createPlatformEndpoint(params, function(err, data) {
        if (err) {
          console.log(err.stack);
          return;
        }

        var endpointArn = data.EndpointArn;

        var payload_pattern = (msg, origin) => {
          return {
            default: '',
            APNS:
              {
                aps:
                {
                  'content-available': 1,
                  alert: msg,
                  badge: 1,
                  category: origin,
                  sound: 'default'
                }
              },
            GCM:
                { notification:
                  { title: msg,
                    body: msg,
                    click_action: origin
                  }
                }
            }
          };

      
        var payload = payload_pattern(msg, origin);
        payload.APNS = JSON.stringify(payload.APNS);
        payload.GCM = JSON.stringify(payload.GCM);

        payload = JSON.stringify(payload);

        console.log('sending push');
        sns.publish({
          Message: payload,
          MessageStructure: 'json',
          TargetArn: endpointArn
        }, function(err, data) {
          if (err) {
            console.log(err.stack);
            return;
          }

          console.log('push sent');
          console.log(data);
        });
      });
}
