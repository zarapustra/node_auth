const mailer = require('./mailer');

module.exports = function (username, pass) {
        return mailer({
            to_email: username,
            file: '/../../views/mailer/signup.ejs',
            subject: 'signup',
            locals: {
                pass: pass,
                username: username
            }
        });
    }
