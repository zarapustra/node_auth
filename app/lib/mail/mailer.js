module.exports = function(params) {
    var ejs = require('ejs'),
        fs = require('fs'),
        file = fs.readFileSync(__dirname + params.file, 'utf-8'),
        rendered = ejs.render(file, params.locals);
    var helper = require('sendgrid').mail;
    // TODO insert email 10levels
    var from_email = new helper.Email('info@10levels.ru');
    var to_email = new helper.Email(params.to_email);

    // TODO subject in different languages
    var subject = params.subject;

    var content = new helper.Content('text/plain', rendered);
    var mail = new helper.Mail(from_email, subject, to_email, content);
    var sg = require('sendgrid')(process.env.SENDGRID_API_KEY);
    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON(),
    });
    return sg.API(request, function(error, response) {
        console.log('#######Signup notify status: ' + response.statusCode);
        //console.log(response.body);
        //console.log(response.headers);
    });
}
