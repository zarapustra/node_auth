const crypto = require('crypto');
const marsha = require('marsha');

const bigKey = '53c1196e2020b45ae9f6ae99464664293b898c29b52eeaec84c86a78aa695f593c24cf835d5ff673f6' +
    'df9d498565f2ba73d9b47c6c850d48d45bacea8aff84ef';
const smallKey = '53c1196e2020b45ae9f6ae9946466429';

exports.encode = (data) => {
    var result;

    var iv = crypto.randomBytes(16);

    var encipher = crypto.createCipheriv('aes-256-cbc', smallKey, iv);
    var tmp = encipher.update(marsha.dump(data), 'utf-8', 'binary');

    tmp += encipher.final('binary');
    var tmpBuffer = new Buffer(tmp, 'binary');
    var encipheredMessage = [tmpBuffer.toString('base64'), iv.toString('base64')].join('--');

    var encipheredMessage64 = Buffer.from(encipheredMessage, 'UTF8').toString('base64');
    var hmac = crypto.createHmac('sha1', bigKey);
    hmac.update(encipheredMessage64);
    var digest = hmac.digest('hex');

    result = [encipheredMessage64, digest].join('--');
    return result;
}

exports.decode = (data) => {
    var result = null;

    var parts = data.split('--');

    if (parts.length === 2 && getHash(parts[0]) === parts[1]) {
        var subParts = Buffer.from(parts[0], 'base64').toString('UTF8').split('--');
        if (subParts.length === 2) {
            var message = Buffer.from(subParts[0], 'base64'),
                iv = Buffer.from(subParts[1], 'base64');

            var decipher = crypto.createDecipheriv('aes-256-cbc', smallKey, iv);
            var decToken = decipher.update(message).toString('UTF8');
            decToken += decipher.final('UTF8');

            result = marsha.load(decToken, 'utf8');
        }
    }
    return result;
}

exports.getHash = (data) => {
    var hmac = crypto.createHmac('sha1', bigKey);
    hmac.update(data);
    return hmac.digest('hex');
}

exports.generateToken = () => {
    return crypto.randomBytes(12).toString('hex');
}
